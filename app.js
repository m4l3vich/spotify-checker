const https = require('https')
const querystring = require('querystring')
console.log('SpotifyChecker working')

function check () {
  console.log('Checking if Spotify is available in Russia...')
  https.get('https://www.spotify.com/ru-ru/', resp => {
    var available = !resp.headers.location.includes('why-not-available')

    console.log(available ? 'OMFG IT IS' : 'Nah.')
    sendMsg(available ? 'Да!' : 'Нет..')
  }).on('error', err => {
    console.log(`Error: ${err.message}`)
  })
}

function sendMsg (txt) {
  var vkUrl = encodeURI(`https://api.vk.com/method/wall.post?owner_id=${process.env.vkgroup}&from_group=1&message=${txt}&access_token=${process.env.vktoken}&v=5.78`)
  var tgUrl = encodeURI(`https://api.telegram.org/bot${process.env.tgtoken}/sendMessage?chat_id=${process.env.tgchannel}&text=${txt}`)

  if (process.env.disable !== 'vk') https.get(vkUrl)
  if (process.env.disable !== 'telegram') https.get(tgUrl)

  if (process.env.disable !== 'vk' && txt !== 'Нет..' && process.env.vkbrtoken && process.env.vkbrlistid) {
    var req = https.request(`https://broadcast.vkforms.ru/api/v2/broadcast?token=${process.env.vkbrtoken}`)

    req.write(
      querystring.stringify({
        message: 'Spotify доступен в России!\nhttps://spotify.com/ru',
        run_now: 1,
        list_ids: 735420
      })
    )
    req.end()
  }
}

setInterval(check, 3600000)
check()
